/*******************************************************************************
 * Copyright (C) 2016 Mango Business Solutions Ltd, [http://www.mango-solutions.com]
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU Affero General Public License as published by the
* Free Software Foundation, version 3.
*
* This program is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
* or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License 
* for more details.
*
* You should have received a copy of the GNU Affero General Public License along 
* with this program. If not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 ******************************************************************************/
package eu.ddmore.converters.nonmem.statements;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eu.ddmore.libpharmml.dom.dataset.DataSet;
import eu.ddmore.libpharmml.dom.dataset.ExternalFile;
import eu.ddmore.libpharmml.dom.dataset.ExternalFile.Delimiter;
import eu.ddmore.libpharmml.dom.trialdesign.ExternalDataSet;

public class DataSetHandlerWithRelativePathTest  {


    private DataSetHandler dataSetHandler;
    private List<ExternalDataSet> extDataSets = new ArrayList<ExternalDataSet>();
    private String dataLocation;
    private String dataFileName;

    @Before
    public void setUp() throws Exception {
        URL dataUrl = System.class.getResource("/test_data_files");
        Path dataPath = Paths.get(dataUrl.toURI());
        dataLocation = dataPath.toAbsolutePath().toString();
        this.dataFileName = "warfarin_conc.csv";
        
        eu.ddmore.libpharmml.dom.dataset.ObjectFactory dsFact = new eu.ddmore.libpharmml.dom.dataset.ObjectFactory();
        eu.ddmore.libpharmml.dom.trialdesign.ObjectFactory tdFact = new eu.ddmore.libpharmml.dom.trialdesign.ObjectFactory();
        
        DataSet dst = dsFact.createDataSetType();
        ExternalFile ef = dsFact.createImportDataType();
        ef.setPath(dataFileName);
        ef.setDelimiter(Delimiter.COMMA);
        ef.setFormat("CSV");
        dst.setExternalFile(ef);
        ExternalDataSet extDataSet = tdFact.createExternalDataSetType();
        extDataSet.setDataSet(dst);
        extDataSets.add(extDataSet);

    }

    @Test
    public void TestGetDataFileName() {

        dataSetHandler = new DataSetHandler(extDataSets, dataLocation);

        assertEquals("Should return expected data file name", dataFileName, dataSetHandler.getDataFileName());
        File actualFile = dataSetHandler.getDataFile();
        assertNotNull(actualFile);
        assertTrue(actualFile.exists());
    }

    @Test
    public void TestConstructedSuccessfully() {
        dataSetHandler = new DataSetHandler(extDataSets, dataLocation);
        assertNotNull(dataSetHandler);
    }

}
